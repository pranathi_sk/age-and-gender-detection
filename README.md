# Age and Gender Detection

**Objective**

To build a gender and age detector that can approximately guess the gender and age of the person in a picture.


**Basic Approach**

- Detect faces
- Classify into Male/Female
- Classify into one of the 8 age ranges
- Put the results on the image and display it

The age ranges we used in this project are (0 – 2), (4 – 6), (8 – 12), (15 – 20), (25 – 32), (38 – 43), (48 – 53), (60 – 100) 


**About the Team**

TEAM - 7

1. M. Lekhya Sri : 19WH1A0582 : CSE
2. S.K. Pranathi : 19WH1A04B4 : ECE
3. K.V.S. Jahnavi : 19WH1A04A9 : ECE
4. T. Sree Aiswarya : 19WH1A0584 : CSE
5. P. Mrunalini : 19WH1A1206 : IT
6. K. Amulya : 19WH1A05F4 : CSE

